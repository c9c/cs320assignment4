Connor Campi

connor@campi.cc

prog4_1 defines the Tokenize function - which takes a string and divides its words into a list - and the Parse function - which takes a Tokenized list and ensures that its syntax is correct.

prog4_2 defines the StackMachine class, the purpose of which is to take Tokenized and Parsed lists and perform arithmetic and stack operations designated by this input.

prog4_3 takes a text file as an argument, Tokenizes and Parses each line, then feeds the result to a StackMachine.
