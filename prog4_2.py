class StackMachine:
	def __init__(self):
		self.stack = Stack()
		self.mem = {}
		self.currentLine = 0
	def Execute(self,tokens):   #Takes Tokenized list (from prog4_1) and performs 
                                		#arithmetic/stack operations based on this input
		func1 = {
			"pop":self.Pop,
			"add":self.Add,
			"sub":self.Sub,
			"mul":self.Mul,
			"div":self.Div,
			"mod":self.Mod,
			"skip":self.Skip,
		}
		func2 ={
			"push":self.Push,
			"save":self.Save,
			"get":self.Get
		}
		for i in range(0,len(tokens)):
			if(tokens[i] in func1):
				call = func1[tokens[i]]
				out = call()
			if(tokens[i] in func2):
				call = func2[tokens[i]]
				out = call(int(tokens[i+1]))
		self.currentLine = self.currentLine + 1
		if out is not None:
			return out
		
	def Push(self,x):   #Add item to stack
		self.stack.Push(x)
	def Pop(self):		#Remove item from stack
		return self.stack.Pop()
	def Add(self):		#Take two items from stack, add together
		val1 = self.stack.Pop()
		val2 = self.stack.Pop()
		self.stack.Push(val1 + val2)
	def Sub(self):		#Take two items from stack, subtract one from other
		val1 = self.stack.Pop()
		val2 = self.stack.Pop()
		self.stack.Push(val1 - val2)
	def Mul(self):		#Take two items from stack, multiply together
		val1 = self.stack.Pop()
		val2 = self.stack.Pop()
		self.stack.Push(val1 * val2)
	def Div(self):		#Take two items from stack, divide one from other
		val1 = self.stack.Pop()
		val2 = self.stack.Pop()
		self.stack.Push(val1 / val2)
	def Mod(self):		#Take two items from stack, divide one from other, use remainder
		val1 = self.stack.Pop()
		val2 = self.stack.Pop()
		self.stack.Push(val1 % val2)
	def Skip(self):					#If first item on stack is 0, add value of 
		val1 = self.stack.Pop()				#second item to loop incrementation
		val2 = self.stack.Pop()
		if(not val1):
			self.currentLine = self.currentLine + val2
	def Save(self,x):				#Take item from stack and place in memory
		self.mem[x] = self.Pop()
	def Get(self,x):				#Take item from memory and place on stack
		if(x not in self.mem):
			raise IndexError("Invalid Memory Access")
		self.stack.Push(self.mem[x])

class Stack:   #Data structure forming the basis for the StackMachine class
	def __init__(self):
		self.data = []
	def Push(self,val):
		self.data.append(val)
	def Pop(self):
		if (len(self.data) == 0):
			raise IndexError("Invalid Memory Access")
		return self.data.pop()
	def Peek(self):
		if (len(self.data) == 0):
			raise IndexError("Invalid Memory Access")
		return self.data[len(self.data)-1]
	def giveMe(self,yourPhone):		#Give Me Your
		self.Push(yourPhone)			#Phone
'''
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMNNNNMMMMMMMddhdMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNNNMMMMNNNNNMMMMMMMMMMMMMMMMMMMMMNNMMMMMMMMMMMNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMms/-..`.-/sNMMN-``-NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN-....-dMMy.....oMMMMMMMMMMMMMMMMMMMMs--.-hMMN+.../NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMy.  `-//:`  .dMMsoosNdyyymMMNyyydMMNhysosydMMMMMMMMMm`     /MN-     /MMMmhysssydNMMMMMMMMNs`  `sN/   /mMMMmhssssymMMMdyyyNMMyyyhMMhyymdssmMMMMMMMMMM
Mm.   sNNNmy+++yMN.  .Nd`  :NMs` `hMo.``-:. `/mMMMMMMMm`  `  `dy  `   /MNo.``-:. `:mMMMMMMMMMd-  `.  `oNMMN+.``--` .+NM+  `dMN.  -MN.  .`  hMMMMMMMMMM
Md`  `mMy......+MN.  .NMs   sm.  sMh`  :os+   /MMMMMMMm`  -:  :- `/   /My`  :ss+   /MNNNNMMMMMm/    .yNMMMo   yNNy   sM/  `dMN`  -MN.  `+hhNMMMMMMMMMM
MN.   oNmys-   /MN.  .NMM+  .:  /NMs   :ooo/::oNNNNMMMm`  -h     o+   /Mo   :oo+/::+NNNNNNMMMMMN.   sMMMMM+   dMMd   oM/   dMN`  -MN.  .NMMMMMMMMMMMMM
MMh.   .--.    /MN.  .NMMN-    -mMMm:  .os+.``oNNMMNNNm`  .N/   .m+   /Nm-  -os+```oNNNNNNNNNNNN.   yMMMMMd-  -oo-  -mMs   -+-`  -MN.  .NMMMMMMMMMMMMM
MMMmy+::::/oh+/sMN+::+NMMMd/::/dMMMMNy+::--:+hNNNNNNNNN/::+Nd:::yNs:::oNNmy/:---:+hNNNNNNNNNNNNN+:::hMMMMMMmy/:--:/ymMMNs/::/sy::+MN+::+MMMMMMMMMMMMMM
MMMMMMMNNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNNNMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmNNNNNNNNNNNNNNNNNNNNNNNNNMMMMMMNNNNMMMMMMMMNNMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNMMNNNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNMNNNNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMNMNNNNNNNNMMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNMNMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMNNNMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNMNMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmmNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmmmmmmmmddddddmmNmmmmNNNNNNNNNNNNNNNmNmmmmmmmmmNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmmmmddhyyysssyhddddddmmmmmmmmmNNNNmmmmmmmmmmmmmmmmNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmmmmmmddhhysoooosyhhddddmmmmmmmmmNNNNmmmmmmmmmmmmmmmmNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmmmmdddhysoo++oyhhhddddmmmddmdmNNNmmmmmmmmmmmmmmmmmmmNNNmNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmmNmmmdddddhyso+/+oyyhhddddmddddddmNNNNNmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmmmmmmmddddhyo+///+ossyhhdddddddddhmmmmNNmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMMM
MMMMMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmmdmmNNNNmmmmddddhhhhyys+///++oosyyhhdddddddhmmdmNNmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMM
MMMMMMMMMNMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmdhddmNNmmmmmddddddhhyssoooo+++ossyhddddmmmmmmmdmNNmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMM
MMMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmmmmmmNNmdhyydNmmmmdyyssssyhhyyssssooossyyhdddhhhhddmNmNNmmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMM
MMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNNNNmNmmmmmmmmmmmmhsoymNmmdhhyyyysssyyyyysssssyyhhhdhhdddmmmdNNNNmmmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNNNNNMMMMMMMMMMMMMMM
MMMMMMMMNNNNNNNNNNNNNNNNNNNNNNNmmmmmmmmmmmmmmmmmmhhhdmNmdhhdhsdmydooyhhyyyyyyyhs+hddd/sddddNNNNmmmmmmmmmmmmmmmmmmmmmmmmNmNNNNNNNNNNNNNNMMMMMMMMMMMMMMM
MMMMMMNNNNNNNNNNNNNNNNNNNNNNNNmmmmmmmmmmmmmmmmmmmmdhydNNdhhhy+oyy+:/+syyysssyhhs+ohdhhddddmNNNNNmmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNNNNNNMMMMMMMMMMMMMM
MMMMMMNMNNNNNNNNNNNNNNNNNNNNNNmmmmmmmmmmmmmmmmmmmmmdhdmNNhhhddhhhhhysdhhhs+oshhyyhhhhhhhhdmmNNmmmmmmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNNNNMMMMMMMMMMMMMM
MMMMMMNNNNNNNNNNNNNNNNNNNNNNNNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmhssossssssysshs+/ohdyhhhhhhhhhddNNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNNNNNMMMMMMMMMMMM
MMMMMMNNNNNNNNNNNNNNNNNNNNNNNNmmmmmmmmmmmmmmmmmmmmmmmmmmmdhhso+/+++ossyyy+//osyhhhhhhhhddmNNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNNNMMMMMMMMMMMM
MMMMMNNNNNNNNNNNNNNNNNNNNNNmNNmmmmmmmmmmmmmmmmmmmmmmmmmmmdhhyoosoossyyyss+//+osyhyyyyyhhdNNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNNNNNMMMMMMMMMMMM
MNNNNNNNNNNNNNNNNNNNNNNNNNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmddhyyssossssyyoo+///ossyyyyyhhhdNNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNNNNNMMMMMMMMMMM
MMNNNNNNNNNNNNNNNNNNNNNNNNNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmddhhhyssssosysooo++osssyyyyhhhdNNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNNNMMMMMMMMMMM
MMNNNNNNNNNNNNNNNNNNNNNNNNNmmmmmmmmmmmmmmmmmmmmmmmmmmmmNmmdddhyyyssossssssssssyyyyhhhdmNNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNMMMMMMMMMMM
MMMNNNNNNNNNNNNNNNNNNNNNNNNmmmmmmmmmmmmmmmmmmmmmmmmmmmmNNmdddhhyyssssssssssssyyyhhhddmNNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNNMMMMMMMMMMM
MMMMNNNNNNNNNNNNNNNNNNNNNNmmmmmmmmmmmmmmmmmmmmmmmmmmdddmNmdddddhhyyyyyyyyyyyyhhhhdddmNNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNMMMMMMMMMMMM
NNNNNNNNNNNNNNNNNNNNNmmmmmmmmmmmmmmmmmmmmmmmmmmmdhys+++oymmddhhhhhhhhyooosyysssyhddmmNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNMMMMMMMMMMMM
MNNNNNNNNNNNNNNNNNNNmmmmmmmmmmmmmmNmmmmmmmmmmmhyo++++////odmdhhhhhhhhhyyssssshhhddmNhyydmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNNMMMMMMMMMMMM
MNNNNNNNNNNNNNNNNNNNmmmmmmmmmmmmmmmmmmdhhhyysooo++++++////+sddhhhhhhhhhhhhhhhhhhddmNNmdhssyhdmmmmmmmmmmmmmmmmmmmmmmmmNNmmmmmmNNNNNNNNNNNNNNMMMMMMMMMMM
MNNMNNNNNNNNNNNNNNmmmmmmmmmmmmmmmdyyysso++///++o++++++++////+sddhhhhhhhhhhhhdhhhddmNmmmmhooooyyhddmmmmmmmmmmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNNMMMMMMMMMMMM
MMMMNNNNNNNNNNNNmNmmmmmmmmmmmhyyo++++++++////////++++/////////+yhdhhhhhhhhhhhyyyhhdmdddddy++oooooosshdmmmmmmmmmmmmmmmmmmNNNNNNNNNNNNNNNNNNMMMMMMMMMMMM
MMMMNMNNNNNNNNNNNNNmNmmmmmhso++///////-.........-///:````///////+shddhhhhhhhhhhhhhmmhhhhho+++++oooooooosyhdmmmmmmmmmmmmmNNNNNNNNNNNNNNNNNMMMMMMMMMMMMM
MMMMNNNNNNNNNNNNNNNNNmmmds++//////////`          `-/:   `//:/://///+syhyyyhhhhhhydddhhys+///+++++++++++++ossyhdmmmmmmmmmmNNNNNNNNNNNNNNNMMMMMMMMMMMMMM
MMMNNNNNNNNNNNNNNNNNNNmyo+////////////`   `//-    ./-    .`````-///-.`````.:+hhh-..:+.````-+++/-.`````.-/++oossydmNNmmmmmmNNNNNNNNNNNNNNNMMMMMMMMMMMMM
MMMNNNNNNNNNNNNNNNNNNmy+//////////////`   `--.    -/-    `.`   `::.   `--.   -hh.   `..    :+:`  `:/:`  `/++oosssydNmmmmmmNNNNNNNNNNNNNNNNMMMMMMMMMMMM
MMMNNNNNNNNNNNNNNNNNNho///////////////`         `-//-    ::-    ::`   :///`  `ss`   -//.   -+.   ```.`   -++++osssymNNmmmNNNNNNNNNNNNNNNNMMMMMMMMMMMMM
MMMNNMNNNNNNNNNNNNNNmo////////////////`   `::::::///-   `::-    ::`   -//:`  `o/`   ://.   -+-   .-..```.-:--/+oooohNNNNNNNNNNNNNNNNNNNNNNMMMMMMMMMMMM
MMMMMNNNNNNNNNNNNNNNd+////////////::::`   `/////////-   `::-    :/-`  ````  `-hs`   ://.   -oo-`  ```   `:---:+oooosmMMMMNNNNNNNNNNNNNNNNNMMMMMMMMMMMM
MMMMMMMNNNNNNNNNNNNmh+//////////::::::-...-:////////:...-:::....:/::-......-:/yd:...://:--:+sss+/-----/+os++/++ooooohNMMMNNNNNNNNNNNNNNNNMMMMMMMMMMMMM
MMMMMMMNNNNNNNNNNNNmh+///////////:::::::::::://///////:::::::::///////////////+h+//////++ossssosooosssyssooooo+oooooshNNMNNNNNNNNNNNNNMMMMMMMMMMMMMMMM
MMMMMMMMNNNNNNNNNNNmy+////////////:::::::::::::///////:::::::::////////////////+///////++ooossooooos++oooooooo++oooosyyydNNNNNNNNNNNMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMNNNNNNNNms///////////////::::::::::://///////:::://///:::::::::/+++++////////++oooooooooo-.-:::://+ooooosssssdNNNNNNNNNNNMNMMMMMMMMMMMMMMMM
MMMMMMMMMMMMNNNNNNNms////////////////////////////::::::///:::------------------::------:/+ossssoo+oo:..::----/o+ooosssyhmNNNNNNNNNNNMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMNNNNNNms+++//////////:::::::::::::::::::::/::-...............................--:+syhysoo-./o+++o/ooooossyyyhNNNNNNNNNNMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMNNNNNNds+++++////////:::::::::::::::::/:::--.....................................-:+yddy+.-////o+ooossssyyyydNNNNNNNNMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMNNNds+++++////////::::::::::::://///--................`..........................-/sdd+-.---:ooosssssyyyyhdNNNNNNMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMNNdo+++++++////////:////++++ooo/:---........`````..``````.........................-:shhs+/:/oossssssyyyyyhmNMMNMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMNdooooo++osossssssssssso+/////:---......`.````````.``..............................-/sdmhsoosssssyyyyyyyhhmNMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMNhssssoo+oydddhhhhyso/:-------...-......``````````.................................---/sddyysssssyyyyhhhhhhmMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMNdysssssooohddddhs/-........................``...............................-..----.---/ymNdysyyyyhhhhhhdmNMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMNmhssssssoshdddy:......................................................------...-...----/hNNmyyyyyhhhhhmNNMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMNmdhyysssydddo..................---.......................-.......--------.......---::/oNMNdyyyhhhyosNMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMNNmdhyhdmo................-:/:-.................--------------------.......--:////osNMNmyyyhdms+oNMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNmmd/-..........--:+yhs+/:--.........--------------------------.-...-::///++osmMMNdhdmNNdshMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNdymmo:-....--:+syhddhsso+:--------------------------------::::::::--://///+oohNMMNNNNNMNmdNMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNhyymNdyo++oshdmmdhhyysssso/::::::::::--------------------::::::///+////::::/+odNMMMMMMMMmdNMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNhyyhmNNmmmmmmmddddhyyyyyyyyso+////:///:::-------------.-:::::::://+o+:-------:+dMMMMMMMMNdNMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNhyyyhmNNmmmmmmmmmdddhhhyyyhdhy+////:::////:---:::/:::--:::::::::::/+/----------sNMMMMMMMMNMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMmyyyyhhdNNNNNNNNmmmmmddhhhhhhdy+/::::::::/++/::///::////+/:::::::::://---.------sNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNdyyyyyhhhmNNNNNNNNNNmmmddddddmy+:::----:::/+o+//////+++oyo/::::::::::/:--..----sNMMMMMMMMMMNMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNdyyyyyhhhyhmNNNNNNNNNNmmmmmmNNh+:----------:/s+/::::::/+syo/::------:::://:-::oNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNhyyyyyyhhyyyhmNNNNNNNNNNNmNNNNdo:------------///:::::://+sy+:----------:+dNNNNNNNNNNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMmhhyyyyyhhhyssshmNNNMMNNNNNNNNNms:-------------:::----:::/oys:------------/dMMMMMMNNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMmhhhyyyyhhdyssssshmNMMMMMMMMNNNNd+-------.----------------:/o:-------------+NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMNmhhhhhhhhhdhyyssyyyhmNMMMMMMMNNNNh/-.--------------------------------------oNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMNdhhhhhhhhhddhyyyyyyyyhNNMMMMMMMMNNdo/--------/:--------------::----------/sNMMMNdNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMNmhhhhhhhhhddhyyyyyyyyyhmNMMMMMMMMMNNmh+///+sdh:-------------:hmho/-----:odNMMNNmyNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMNmdddhhhhhhhddhyyyyyyyyyydMMMMMMMMMMMNNNNNNNNNmo:-----------+mMMMNdysooshmNNNdmmmdmMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMNmdddddddhhhdmdhhhhhhhyyhhmNMMMMMMMMMMMMMMNMMNMNmhs+/::-::/sNMMNmmhhhhhhdmmmmmNmmmmMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMNmddddddddddddmmdhhhhhhhhhhmMMMMMMMMMMMMMMMMMMMMMMMNNmdyssyhddddddddddddddmmNNNNNNmNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
'''
